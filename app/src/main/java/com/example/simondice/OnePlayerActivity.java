package com.example.simondice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import Controllers.Controlador;

public class OnePlayerActivity extends AppCompatActivity {
    private Button btn1, btn2, btn3, btn4, btn5, btn6;
    private TextView puntuacionTV;
    private int index, puntuacion;
    private Controlador ctrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.ctrl = new Controlador();
        this.index = 0;
        this.puntuacion = 0;

        //Bindeo Botones
        this.btn1 = findViewById(R.id.btn1Verde);
        this.btn2 = findViewById(R.id.btn2Amarillo);
        this.btn3 = findViewById(R.id.btn3Naranja);
        this.btn4 = findViewById(R.id.btn4Fucshia);
        this.btn5 = findViewById(R.id.btn5Malva);
        this.btn6 = findViewById(R.id.btn6Morado);
        //Bindeo Puntuacion
        this.puntuacionTV = findViewById(R.id.puntuacionTV);

        //Damos valor al TexTView
        this.puntuacionTV.setText(String.valueOf(this.puntuacion));

        //Función que nos muestra el patrón de los botones por cada turno
        reproducirPatron();


        //--------------------------------
        //  EVENTOS BOTONES - LISTENERS
        //--------------------------------
        //BOTÓN 1
        this.btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (index < ctrl.getListaPatron().size()) {
                    boolean patronCorrecto = ctrl.comprobarPatron(1, index);

                    if (patronCorrecto && index == (ctrl.getListaPatron().size() - 1)) {
                        puntuacion++;
                        puntuacionTV.setText(String.valueOf(puntuacion));
                        ctrl.addPatronTablero();
                        index = 0;
                        reproducirPatron();

                    } else {
                        index++;
                    }

                    if (!patronCorrecto) {
                        puntuacion = 0;
                        index = 0;
                        puntuacionTV.setText(String.valueOf(puntuacion));
                        Toast msjFin = Toast.makeText(getApplicationContext(), "FIN DEL JUEGO", Toast.LENGTH_LONG);
                        msjFin.show();
                    }
                }
            }
        });

        //BOTÓN 2
        this.btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (index < ctrl.getListaPatron().size()) {
                    boolean patronCorrecto = ctrl.comprobarPatron(2, index);

                    if (patronCorrecto && index == (ctrl.getListaPatron().size() - 1)) {
                        puntuacion++;
                        index = 0;
                        puntuacionTV.setText(String.valueOf(puntuacion));
                        ctrl.addPatronTablero();
                        reproducirPatron();
                    } else {
                        index++;
                    }

                    if (!patronCorrecto) {
                        puntuacion = 0;
                        index = 0;
                        puntuacionTV.setText(String.valueOf(puntuacion));
                        Toast msjFin = Toast.makeText(getApplicationContext(), "FIN DEL JUEGO", Toast.LENGTH_LONG);
                        msjFin.show();
                    }
                }
            }
        });

        //BOTÓN 3
        this.btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (index < ctrl.getListaPatron().size()) {
                    boolean patronCorrecto = ctrl.comprobarPatron(3, index);

                    if (patronCorrecto && index == (ctrl.getListaPatron().size() - 1)) {
                        puntuacion++;
                        index = 0;
                        puntuacionTV.setText(String.valueOf(puntuacion));
                        ctrl.addPatronTablero();
                        reproducirPatron();
                    } else {
                        index++;
                    }

                    if (!patronCorrecto) {
                        puntuacion = 0;
                        index = 0;
                        puntuacionTV.setText(String.valueOf(puntuacion));
                        Toast msjFin = Toast.makeText(getApplicationContext(), "FIN DEL JUEGO", Toast.LENGTH_LONG);
                        msjFin.show();
                    }
                }
            }
        });

        //BOTÓN 4
        this.btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (index < ctrl.getListaPatron().size()) {
                    boolean patronCorrecto = ctrl.comprobarPatron(4, index);

                    if (patronCorrecto && index == (ctrl.getListaPatron().size() - 1)) {
                        puntuacion++;
                        index = 0;
                        puntuacionTV.setText(String.valueOf(puntuacion));
                        ctrl.addPatronTablero();
                        reproducirPatron();
                    } else {
                        index++;
                    }

                    if (!patronCorrecto) {
                        puntuacion = 0;
                        index = 0;
                        puntuacionTV.setText(String.valueOf(puntuacion));
                        Toast msjFin = Toast.makeText(getApplicationContext(), "FIN DEL JUEGO", Toast.LENGTH_LONG);
                        msjFin.show();
                    }
                }
            }
        });

        //BOTÓN 5
        this.btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (index < ctrl.getListaPatron().size()) {
                    boolean patronCorrecto = ctrl.comprobarPatron(5, index);

                    if (patronCorrecto && index == (ctrl.getListaPatron().size() - 1)) {
                        puntuacion++;
                        index = 0;
                        puntuacionTV.setText(String.valueOf(puntuacion));
                        ctrl.addPatronTablero();
                        reproducirPatron();
                    } else {
                        index++;
                    }

                    if (!patronCorrecto) {
                        puntuacion = 0;
                        index = 0;
                        puntuacionTV.setText(String.valueOf(puntuacion));
                        Toast msjFin = Toast.makeText(getApplicationContext(), "FIN DEL JUEGO", Toast.LENGTH_LONG);
                        msjFin.show();
                    }
                }
            }
        });

        //BOTÓN 6
        this.btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (index < ctrl.getListaPatron().size()) {
                    boolean patronCorrecto = ctrl.comprobarPatron(6, index);

                    if (patronCorrecto && index == (ctrl.getListaPatron().size() - 1)) {
                        puntuacion++;
                        index = 0;
                        puntuacionTV.setText(String.valueOf(puntuacion));
                        ctrl.addPatronTablero();
                        reproducirPatron();
                    } else {
                        index++;
                    }

                    if (!patronCorrecto) {
                        puntuacion = 0;
                        index = 0;
                        puntuacionTV.setText(String.valueOf(puntuacion));
                        Toast msjFin = Toast.makeText(getApplicationContext(), "FIN DEL JUEGO", Toast.LENGTH_LONG);
                        msjFin.show();
                    }
                }
            }
        });
    }

    //Funciones


    //Función para el seguimiento del patrón
    private void reproducirPatron() {
        int millisInFuture = 760;
        int countDownInterval = 760;
        Contador contador = new Contador(millisInFuture, countDownInterval, 0);
        contador.start();

    }


    // CLASE QUE NOS AYUDARÁ CON EL INTERVALO EN EL QUE SE VE EL PATRÓN
    public class Contador extends CountDownTimer {

        private int indiceActual;
        private int colorOriginal;
        private long millisInFuture;
        private long countDownInterval;

        public Contador(long millisInFuture, long countDownInterval, int indiceActual) {
            super(millisInFuture, countDownInterval);
            this.indiceActual = indiceActual;
            this.millisInFuture = millisInFuture;
            this.countDownInterval = countDownInterval;
        }

        @Override
        public void onFinish() {
            switch (ctrl.getListaPatron().get(indiceActual)) {
                case 1:
                    btn1.setBackgroundColor(colorOriginal);
                    break;

                case 2:
                    btn2.setBackgroundColor(colorOriginal);
                    break;

                case 3:
                    btn3.setBackgroundColor(colorOriginal);
                    break;

                case 4:
                    btn4.setBackgroundColor(colorOriginal);
                    break;

                case 5:
                    btn5.setBackgroundColor(colorOriginal);
                    break;

                case 6:
                    btn6.setBackgroundColor(colorOriginal);
                    break;
            }

            if (indiceActual < ctrl.getListaPatron().size() - 1) {
                Contador contador = new Contador(millisInFuture, countDownInterval, indiceActual + 1);
                contador.start();
            }
        }

        @Override
        public void onTick(long millisUntilFinished) {

            switch (ctrl.getListaPatron().get(indiceActual)) {
                case 1:
                    colorOriginal = ContextCompat.getColor(getApplicationContext(), R.color.softGreen);
                    btn1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                    break;

                case 2:
                    colorOriginal = ContextCompat.getColor(getApplicationContext(), R.color.golden);
                    btn2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                    break;

                case 3:
                    colorOriginal = ContextCompat.getColor(getApplicationContext(), R.color.orangina);
                    btn3.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                    break;

                case 4:
                    colorOriginal = ContextCompat.getColor(getApplicationContext(), R.color.fucshia);
                    btn4.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                    break;

                case 5:
                    colorOriginal = ContextCompat.getColor(getApplicationContext(), R.color.malva);
                    btn5.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                    break;

                case 6:
                    colorOriginal = ContextCompat.getColor(getApplicationContext(), R.color.lavanda);
                    btn6.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                    break;
            }
        }
    }
}

