package Controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Controlador {
    private List<Integer> patronTablero;

    //Constructor
    public Controlador(){
        this.patronTablero = new ArrayList<Integer>();
        addPatronTablero();
    }

    //Getter
    public List<Integer> getListaPatron(){
        return this.patronTablero;
    }

    //Añade un número más al patron
    public void addPatronTablero(){
        Random rand = new Random();
        this.patronTablero.add(rand.nextInt(6-1) + 1);
    }


    //Nos comprueba si el patrón es correcto
    public boolean comprobarPatron(int num, int index) {
        boolean patronCorrecto = false;

        if(num == this.patronTablero.get(index)){
            patronCorrecto = true;
        }
        return patronCorrecto;
    }
}
